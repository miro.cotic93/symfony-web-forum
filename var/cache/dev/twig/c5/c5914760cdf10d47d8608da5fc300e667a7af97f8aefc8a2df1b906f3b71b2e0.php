<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* reddit/test.html.twig */
class __TwigTemplate_f1a55305b73b6c8376df06896e3849c13eb959d3e967752efe63278f14c63f55 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "reddit/index.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "reddit/test.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "reddit/test.html.twig"));

        $this->parent = $this->loadTemplate("reddit/index.html.twig", "reddit/test.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 4
    public function block_title($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo " Login ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 6
    public function block_body($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        echo " 
<center>
";
        // line 8
        if (($context["error"] ?? $this->getContext($context, "error"))) {
            echo " 
\t<div class=\"alert alert-danger\"> 
\t\t";
            // line 10
            echo twig_escape_filter($this->env, $this->getAttribute(($context["error"] ?? $this->getContext($context, "error")), "messageKey", []), "html", null, true);
            echo "
\t</div>
";
        }
        // line 12
        echo " 
<div class=\"panel-heading\">
\t<strong><h2 class=\"text-center\"> LOGIN </h2></strong>
</div>
\t<form action=\"";
        // line 16
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("log");
        echo "\" method=\"post\">
\t\t<div class=\"form-group\">
\t\t\t
\t\t\t<label for=\"username\">Username:    </label>
\t\t\t<input type=\"text\" name=\"_username\" id=\"username\"  size=\"5\" class=\"form-control\" value=\"";
        // line 20
        echo twig_escape_filter($this->env, ($context["username"] ?? $this->getContext($context, "username")), "html", null, true);
        echo "\"> </input>
\t\t</div>
\t\t<div class=\"form-group\">
\t\t\t<label for=\"username\"> Password:  </label>
\t\t\t<input type=\"password\" name=\"_password\" id=\"password\" class=\"form-control\" maxlength=\"4\" size=\"4\">    </input>
\t\t</div>
\t\t<div class=\"form-group\">
\t\t\t<label class=\"form-check-label\">
     \t\t \t<input class=\"form-check-input\" type=\"checkbox\"> Remember me
   \t\t\t </label>
   \t\t</div>
\t\t<button class=\"btn btn-info pull-right\"> Connect </button>
\t</form>

</center>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "reddit/test.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  106 => 20,  99 => 16,  93 => 12,  87 => 10,  82 => 8,  70 => 6,  52 => 4,  30 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'reddit/index.html.twig' %}


{% block title %} Login {% endblock %}

{% block body %} 
<center>
{% if error %} 
\t<div class=\"alert alert-danger\"> 
\t\t{{ error.messageKey }}
\t</div>
{% endif %} 
<div class=\"panel-heading\">
\t<strong><h2 class=\"text-center\"> LOGIN </h2></strong>
</div>
\t<form action=\"{{ path('log') }}\" method=\"post\">
\t\t<div class=\"form-group\">
\t\t\t
\t\t\t<label for=\"username\">Username:    </label>
\t\t\t<input type=\"text\" name=\"_username\" id=\"username\"  size=\"5\" class=\"form-control\" value=\"{{ username }}\"> </input>
\t\t</div>
\t\t<div class=\"form-group\">
\t\t\t<label for=\"username\"> Password:  </label>
\t\t\t<input type=\"password\" name=\"_password\" id=\"password\" class=\"form-control\" maxlength=\"4\" size=\"4\">    </input>
\t\t</div>
\t\t<div class=\"form-group\">
\t\t\t<label class=\"form-check-label\">
     \t\t \t<input class=\"form-check-input\" type=\"checkbox\"> Remember me
   \t\t\t </label>
   \t\t</div>
\t\t<button class=\"btn btn-info pull-right\"> Connect </button>
\t</form>

</center>
{% endblock %}", "reddit/test.html.twig", "C:\\xampp\\htdocs\\my_project\\app\\Resources\\views\\reddit\\test.html.twig");
    }
}
